# ics-ans-role-kibana

Ansible role to install kibana.

## Role Variables

```yaml
kibana_gpg_key: https://packages.elastic.co/GPG-KEY-elasticsearch

centos_mirror_url: https://artifacts.elastic.co

kibana_version: 7.x

kibana_conf_template:
  - name: config
    file: kibana.yml.j2
    dest: /etc/kibana/kibana.yml

kibana_repository:
  - baseurl: "{{ centos_mirror_url }}/packages/{{ kibana_version }}/yum"
    description: "Elastic repository for {{ kibana_version }} packages"
    enabled: true
    gpgcheck: true
    gpgkey: "{{ centos_mirror_url }}/GPG-KEY-elasticsearch"
    name: kibana
    reposdir: /etc/yum.repos.d/

elasticsearch_host: http://localhost:9200

kibana_server_port: 5601

kibana_server_host: "{% if ansible_fqdn %}{{ ansible_fqdn }}{% elif ansible_default_ipv4.address %}{{ ansible_default_ipv4.address }}{% endif %}"

...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-kibana
```

## License

BSD 2-clause
